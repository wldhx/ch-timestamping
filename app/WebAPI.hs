module WebAPI (server) where

import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Strict as HMap
import qualified Data.Set as Set
import qualified CRDT.Cv.GSet as GSet
import Control.Monad.IO.Class
import Control.Monad.STM
import Control.Concurrent.STM.TVar
import Servant (Handler, Server, throwError)
import Servant.API
import Servant.Server
import qualified Crypto.Saltine.Class as Saltine
import Crypto.Saltine.Core.Hash
import Crypto.Saltine.Core.Sign
import WebAPI.Types
import Types
import Saltine

addh :: TVar State -> PublicKey -> PHSMsg -> Handler NoContent
addh state pkey (PHSMsg pkey' h s) = if signVerifyDetached pkey s $ Saltine.encode h
    then liftIO . atomically $ modifyTVar state (HMap.insertWith Set.union h $ Set.singleton (pkey', s)) >> return NoContent
    else throwError $ err403 { errBody = "Invalid signature or signer." }

geth :: TVar State -> Handler [String]
geth state = (fmap . fmap) show (HMap.toList <$> (liftIO . readTVarIO $ state))

lookuph :: TVar State -> PHMsg -> Handler Bool
lookuph state (PHMsg pkey h) = do
    v <- HMap.lookup h <$> (liftIO . readTVarIO $ state)
    case v of
        Nothing -> throwError $ err416 { errBody = "No such H found." }
        Just s -> return $ not . Set.null . Set.filter ((== pkey) . fst) $ s

server state pkey =
         addh state pkey
    :<|> geth state
    :<|> lookuph state
