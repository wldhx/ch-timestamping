module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (forever, void)
import qualified Control.Exception as E
import qualified Data.ByteString.Char8 as C
import Control.Monad.STM
import Control.Concurrent.STM.TVar
import Control.Distributed.Process
import Control.Distributed.Process.Node
import Network.Transport (EndPointAddress(..), closeTransport)
import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Data.Maybe (fromJust)
import Data.Binary.Orphans ()
import qualified Data.ByteString as B
import qualified Data.HashMap.Strict as HMap
import qualified Data.Set as Set
import CRDT.Cv.GSet (GSet)
import qualified CRDT.Cv.GSet as GSet
import Crypto.Saltine
import Crypto.Saltine.Class
import Crypto.Saltine.Core.Sign
import Crypto.Saltine.Core.Hash (Hash)
import WebAPI
import WebAPI.Types
import qualified JsonRpcAPI
import Types
import qualified Network.Wai.Handler.Warp as Warp
import Servant (serve)
import qualified Control.Remote.Monad.JSON.Server as RMJSON

-- from distributed-process-p2p
-- | Make a NodeId from "host:port" string.
makeNodeId :: String -> NodeId
makeNodeId addr = NodeId . EndPointAddress . C.concat $ [C.pack addr, ":0"]

main :: IO ()
main = run "9000" "9001"

run p1 p2 = do
  sodiumInit

  pkey <- fromJust . decode @PublicKey <$> B.readFile "public.key"

  state <- newTVarIO $ HMap.empty @Hash @(GSet PS)

  E.bracket ((\(Right x) -> x) <$> createTransport "127.0.0.1" p1 (\p -> ("127.0.0.1", p)) defaultTCPParameters) closeTransport $ \t -> do

    E.bracket (newLocalNode t initRemoteTable) closeLocalNode $ \node -> runProcess node $ do

      void . spawnLocal $ liftIO . Warp.run 8000 $ serve api $ server state pkey

      void . spawnLocal $ liftIO . RMJSON.serverReceiveAPI 8001 "/" $ JsonRpcAPI.server state pkey

      self <- getSelfPid
      register "tstSrv" self

      let other = makeNodeId $ "127.0.0.1:" ++ p2
      say $ show other

      void . spawnLocal $ forever $ do
        liftIO $ threadDelay 200000
        nsendRemote other "tstSrv" =<< (liftIO . readTVarIO $ state)

      forever $ do
        m <- expect @(HMap.HashMap Hash (GSet PS))
        say $ show m
        liftIO . atomically $ modifyTVar' state (HMap.union m)
