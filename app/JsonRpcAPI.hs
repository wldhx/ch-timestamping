module JsonRpcAPI (server) where

import Control.Natural ((:~>), wrapNT)
import Control.Remote.Monad.JSON
import Control.Remote.Monad.JSON.Router
import Data.Aeson
import Data.Text(Text)
import Control.Monad.IO.Class
import Control.Monad.STM
import Control.Concurrent.STM.TVar
import qualified Data.Set as Set
import qualified Data.HashMap.Strict as HMap
import qualified Crypto.Saltine.Class as Saltine
import Crypto.Saltine.Core.Sign
import Types

{-
type API = "add" :> ReqBody '[OctetStream] PHSMsg :> Put '[JSON] NoContent
      :<|> "get" :> Get '[JSON] [String]
      :<|> "lookup" :> ReqBody '[OctetStream] PHMsg :> Get '[JSON] Bool
-}

network :: TVar State -> PublicKey -> SendAPI :~> IO
network state pkey = transport $ server state pkey

server :: TVar State -> PublicKey -> ReceiveAPI :~> IO
server state pkey = router sequence $ wrapNT $ remote state pkey

remote :: TVar State -> PublicKey -> Call a -> IO a
remote state pkey (CallMethod "add" (List [v])) =
    let (PHSMsg pkey' h s) = (\(Success a) -> a) . fromJSON @PHSMsg $ v
    in if signVerifyDetached pkey s $ Saltine.encode h
        then liftIO . atomically $ modifyTVar state (HMap.insertWith Set.union h $ Set.singleton (pkey', s)) >> (return . toJSON $ ())
        else invalidParams
remote state pkey(CallMethod "get" _) = do
    x <- liftIO . readTVarIO $ state
    return $ toJSON $ toJSON <$> HMap.toList x
remote state pkey (CallMethod "lookup" (List [v])) = do
    let (PHMsg pkey h) = (\(Success a) -> a) . fromJSON @PHMsg $ v
    v <- HMap.lookup h <$> (liftIO . readTVarIO $ state)
    case v of
        Nothing -> invalidParams
        Just s -> return . Bool $ not . Set.null . Set.filter ((== pkey) . fst) $ s
remote _ _ _  = methodNotFound
