let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/nixos-18.03.tar.gz") {
    overlays = [
      (self: super: {
        haskellPackages = super.haskellPackages.override (oldArgs: {
          overrides =
            self.lib.composeExtensions (oldArgs.overrides or (_: _: {}))
              (hself: hsuper: {
                saltine =
                  hsuper.callPackage ../saltine { };

                remote-monad =
                  hsuper.callPackage ../remote-monad { };

                remote-json =
                  hsuper.callPackage ../remote-json { };

                remote-json-server =
                  hsuper.callPackage ../remote-json-server { };

                remote-json-client =
                  hsuper.callPackage ../remote-json-client { };
              });
        });
      })
    ];
  };

in
  pkgs.haskellPackages.callPackage ./ch-timestamping.nix { }
