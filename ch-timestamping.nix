{ mkDerivation, aeson, base, binary, binary-orphans, bytestring
, containers, crdt, distributed-process, hashable, http-client
, memory, natural-transformation, network-transport
, network-transport-tcp, remote-json, remote-json-client
, remote-json-server, saltine, servant, servant-client
, servant-server, stdenv, stm, text, unordered-containers, wai
, warp
}:
mkDerivation {
  pname = "ch-timestamping";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base binary bytestring crdt hashable memory remote-json
    saltine servant servant-client text unordered-containers
  ];
  executableHaskellDepends = [
    aeson base binary-orphans bytestring containers crdt
    distributed-process http-client natural-transformation
    network-transport network-transport-tcp remote-json
    remote-json-client remote-json-server saltine servant
    servant-client servant-server stm text unordered-containers wai
    warp
  ];
  description = "A Cloud Haskell GSet-based timestamping service";
  license = stdenv.lib.licenses.agpl3;
}
