module WebAPI.Client where

import Servant.API.Alternative
import Servant.Client
import WebAPI.Types

add :<|> get :<|> lookup = client api
