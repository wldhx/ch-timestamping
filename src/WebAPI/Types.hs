module WebAPI.Types where

import Data.ByteString (ByteString)
import Data.Proxy
import Servant.API
import Crypto.Saltine.Core.Sign (PublicKey)
import Types

type API = "add" :> ReqBody '[OctetStream] PHSMsg :> Put '[JSON] NoContent
      :<|> "get" :> Get '[JSON] [String]
      :<|> "lookup" :> ReqBody '[OctetStream] PHMsg :> Get '[JSON] Bool

api :: Proxy API
api = Proxy
