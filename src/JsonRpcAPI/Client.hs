module JsonRpcAPI.Client where

import Control.Remote.Monad.JSON
import Data.Aeson
import CRDT.Cv.GSet (GSet)
import Crypto.Saltine.Core.Hash (Hash)
import Types

add :: PHSMsg -> RPC ()
add phsmsg = method "add" $ List [toJSON phsmsg]

get :: RPC [(Hash, GSet PS)]
get = method "get" None

lookup :: PHMsg -> RPC Bool
lookup phmsg = method "lookup" $ List [toJSON phmsg]
