module Saltine where

import Data.Binary (Binary)
import qualified Data.ByteString as B
import Data.Semigroup ((<>))
import Crypto.Saltine.Class (encode)
import Crypto.Saltine.Core.Sign (PublicKey)

instance Show PublicKey where
    show x = show ((B.take 4 . encode $ x) <> "...")

instance Binary PublicKey
