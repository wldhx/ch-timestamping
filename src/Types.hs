module Types where

import Data.Aeson (ToJSON, FromJSON, toJSON, fromJSON, parseJSON, Value(..), Result)
import Data.Binary
import Data.Hashable (Hashable)
import Data.ByteString (ByteString)
import CRDT.Cv.GSet (GSet)
import qualified Data.HashMap.Strict as HMap
import qualified Crypto.Saltine.Class as Saltine
import Crypto.Saltine.Core.Sign (PublicKey, Signature)
import Crypto.Saltine.Core.Hash (Hash)
import GHC.Generics (Generic)
import Servant.API.ContentTypes
import Saltine ()
import Util

type PHS = (PublicKey, Hash, Signature)
type PS = (PublicKey, Signature)
type State = HMap.HashMap Hash (GSet PS)

instance ToJSON PublicKey where
  toJSON = String . hexT . Saltine.encode

instance FromJSON PublicKey where
  -- FIXME
  parseJSON (String v) = do
    y <- case unhexT v of
      Left e -> fail e
      Right v -> return v
    case Saltine.decode y of
      Nothing -> fail "Saltine.decode failed"
      Just v -> return v

instance Binary Hash
instance Hashable Hash

instance ToJSON Hash where
  toJSON = String . hexT . Saltine.encode

instance FromJSON Hash where
  -- FIXME
  parseJSON (String v) = do
    y <- case unhexT v of
      Left e -> fail e
      Right v -> return v
    case Saltine.decode y of
      Nothing -> fail "Saltine.decode failed"
      Just v -> return v

instance Binary Signature

instance ToJSON Signature where
  toJSON = String . hexT . Saltine.encode

instance FromJSON Signature where
  -- FIXME
  parseJSON (String v) = do
    y <- case unhexT v of
      Left e -> fail e
      Right v -> return v
    case Saltine.decode y of
      Nothing -> fail "Saltine.decode failed"
      Just v -> return v

data PHSMsg = PHSMsg PublicKey Hash Signature
    deriving (Show, Generic)

instance Binary PHSMsg
instance ToJSON PHSMsg
instance FromJSON PHSMsg

instance MimeRender OctetStream PHSMsg where
  mimeRender _ = encode

instance MimeUnrender OctetStream PHSMsg where
  mimeUnrender _ = decodeToEither

data PHMsg = PHMsg PublicKey Hash
    deriving (Show, Generic)

instance Binary PHMsg
instance ToJSON PHMsg
instance FromJSON PHMsg

instance MimeRender OctetStream PHMsg where
  mimeRender _ = encode

instance MimeUnrender OctetStream PHMsg where
  mimeUnrender _ = decodeToEither

instance MimeRender OctetStream PublicKey where
  mimeRender _ = encode

instance MimeUnrender OctetStream PublicKey where
  mimeUnrender _ = decodeToEither
