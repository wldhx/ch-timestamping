module Util where

import Data.Binary (Binary, decodeOrFail)
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Encoding as T.E
import qualified Data.ByteArray.Encoding as BA.E

-- from Network.Haskoin.Util
-- | Decode a Data.Binary value into the Either monad. A Right value is returned
-- with the result upon success. Otherwise a Left value with the error message
-- is returned.
decodeToEither :: Binary a => BL.ByteString -> Either String a
decodeToEither bs = case decodeOrFail bs of
    Left  (_,_,err) -> Left err
    Right (_,_,res) -> Right res


base16 :: ByteString -> ByteString
base16 = BA.E.convertToBase BA.E.Base16

unbase16 :: ByteString -> Either String ByteString
unbase16 = BA.E.convertFromBase BA.E.Base16

hexT :: ByteString -> Text
hexT = T.E.decodeLatin1 . base16

unhexT :: Text -> Either String ByteString
unhexT = unbase16 . T.E.encodeUtf8
