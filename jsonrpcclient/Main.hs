module Main where

import Data.Maybe (fromJust)
import qualified Data.ByteString as B
import Control.Remote.Monad.JSON
import Control.Remote.Monad.JSON.Client
import Crypto.Saltine
import Crypto.Saltine.Class
import Crypto.Saltine.Core.Sign
import Crypto.Saltine.Core.Hash
import qualified JsonRpcAPI.Client as API
import Types

main :: IO ()
main = do 
  sodiumInit

  s <- fromJust . decode @SecretKey <$> B.readFile "secret.key"
  p <- fromJust . decode @PublicKey <$> B.readFile "public.key"

  let session = weakSession (clientSendAPI "http://localhost:8001/")
  (t,u,o) <- send session $ do
    t <- API.get
    u <- API.lookup $ PHMsg p $ hash "x"
    o <- API.add $ PHSMsg p (hash "x") (signDetached s $ encode $ hash "x")
    return (t,u,o)
  print t
  print u
  print o
