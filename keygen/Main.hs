module Main where

import qualified Data.ByteString as B
import Crypto.Saltine
import Crypto.Saltine.Class
import Crypto.Saltine.Core.Sign

main :: IO ()
main = do
    sodiumInit

    (s, p) <- newKeypair

    B.writeFile "secret.key" . encode $ s
    B.writeFile "public.key" . encode $ p
