module Main where

import Data.Maybe (fromJust)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Control.Monad (void)
import Network.HTTP.Client
import Servant.Client
import WebAPI.Client
import Crypto.Saltine
import Crypto.Saltine.Class
import Crypto.Saltine.Core.Sign
import Crypto.Saltine.Core.Hash
import Types

main :: IO ()
main = do
    sodiumInit

    s <- fromJust . decode @SecretKey <$> B.readFile "secret.key"
    p <- fromJust . decode @PublicKey <$> B.readFile "public.key"

    manager <- newManager defaultManagerSettings
    let env = ClientEnv manager (BaseUrl Http "localhost" 8000 "")

    i <- C.getLine
    let h = hash i
    let o = PHSMsg p h (signDetached s $ encode h)
    void $ flip runClientM env . add $ o

    (flip runClientM env . WebAPI.Client.lookup $ PHMsg p (hash i)) >>= print
